//
//  AuthVC.m
//  ISY Chat
//
//  Created by Михаил Луцкий on 19.08.15.
//  Copyright (c) 2015 LWTS Technologies. All rights reserved.
//

#import "AuthVC.h"
#import <AFNetworking/AFNetworking.h>
#import "UserCache.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIColor+ISYColors.h"
#import "UIAlertMessage.h"

@interface AuthVC () <MBProgressHUDDelegate, UITextFieldDelegate> {
    MBProgressHUD *HUD;
    bool isKeyboardShow;
}
@property (assign) UITapGestureRecognizer *tapRecognizer;
@property (strong, nonatomic) IBOutlet UIView *viewMain;
@end

@implementation AuthVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [_loginButton.layer setCornerRadius:3];
    [_signupButton.layer setCornerRadius:3];
    _passwordField.delegate = self;
    _emailField.delegate = self;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    // do whatever you have to do
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)loginAction:(id)sender {
    [self checkForEmptyFields:kAPIauth];
}

- (IBAction)signupAction:(id)sender {
    [self checkForEmptyFields:kAPIreg];
}

- (IBAction)privacyPolitic:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://bigbadbird.ru/around-me/privacy_policy.php?lang=%@", NSLocalizedString(@"en", nil)]]];
}

- (void) checkForEmptyFields:(NSString*)identifierButton {
    if (![_emailField.text isEqualToString:@""]&&![_passwordField.text isEqualToString:@""]) {
        HUD = [[MBProgressHUD alloc] initWithView:self.view];
        [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
        HUD.dimBackground = NO;
        HUD.color = [UIColor ArounMeColor];
        HUD.delegate = self;
        [HUD show:YES];
        [self makeRequest:identifierButton];
    }
    else
        [self showMessage:NSLocalizedString(@"Fill in all fields.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
}
- (void) makeRequest:(NSString*)typeRequest {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"login": _emailField.text, @"password": _passwordField.text};
    NSLog(@"%@%@", kBaseAPIurl, typeRequest);
    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, typeRequest] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [HUD hide:YES];
        if ([[responseObject valueForKey:@"status"] isEqualToString:@"failed"]){
            if ([typeRequest isEqualToString:kAPIauth]) {
                [self showMessage:NSLocalizedString(@"It seems the entered username or password is incorrect.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
            }
            else {
                [self showMessage:NSLocalizedString(@"It seems user with this login already exists.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
            }
            NSLog(@"JSON: %@", responseObject);
        }
        else {
            NSArray *UserData = [responseObject valueForKey:@"response"][0];
            [NSUserDefaults changeAuth:true];
            [NSUserDefaults changeShowNews:true];
            [NSUserDefaults setValue:[UserData valueForKey:@"token"] forKey:@"Token"];
            [NSUserDefaults setValue:[UserData valueForKey:@"user_id"] forKey:@"User_ID"];
            [NSUserDefaults setValue:[UserData valueForKey:@"avatar_url"] forKey:@"Picture"];
            [NSUserDefaults setValue:[UserData valueForKey:@"type"] forKey:@"Type"];
            [NSUserDefaults setValue:_emailField.text forKey:@"Login"];
            [self dismissViewControllerAnimated:YES completion:nil];
            [self.navigationController popViewControllerAnimated:YES];
            NSLog(@"JSON: %@", responseObject);
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HUD hide:YES];
        [self showMessage:NSLocalizedString(@"Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
        NSLog(@"Error: %@", error);
    }];
}
@end
