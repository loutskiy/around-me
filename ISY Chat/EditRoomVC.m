//
//  EditRoomVC.m
//  Around me
//
//  Created by Михаил Луцкий on 07/10/15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "EditRoomVC.h"
#import <DBMapSelectorViewController/DBMapSelectorManager.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <CoreLocation/CoreLocation.h>
#import "Constants.h"
#import "UserCache.h"
#import "UIAlertMessage.h"
#import "UIColor+ISYColors.h"

@interface EditRoomVC ()  <DBMapSelectorManagerDelegate,MBProgressHUDDelegate,CLLocationManagerDelegate, UITextFieldDelegate> {
    float Radius;
    MBProgressHUD *HUD;
    float latUser, lonUser;
    float latitude, longitude;
    bool isKeyboardShow;
}
@property (nonatomic, strong) DBMapSelectorManager      *mapSelectorManager;
@property (assign) UITapGestureRecognizer *tapRecognizer;
@property (strong, nonatomic) IBOutlet UIView *viewMain;
@end

@implementation EditRoomVC
- (DBMapSelectorManager *)mapSelectorManager {
    if (nil == _mapSelectorManager) {
        _mapSelectorManager = [[DBMapSelectorManager alloc] initWithMapView:self.mapView];
        _mapSelectorManager.delegate = self;
    }
    return _mapSelectorManager;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]]) {
        [self.view endEditing:YES];
    }
    [super touchesBegan:touches withEvent:event];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    // do whatever you have to do
    
    [textField resignFirstResponder];
    return YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _mapView.showsUserLocation = YES;
    _mapView.delegate = self;
    // Set map selector settings
    self.mapSelectorManager.circleCoordinate = CLLocationCoordinate2DMake([_Latitude doubleValue], [_Longitude doubleValue]);
    self.mapSelectorManager.circleRadius = [_radius integerValue]*1000;
    self.mapSelectorManager.circleRadiusMax = 25000;
    [self.mapSelectorManager applySelectorSettings];
    self.mapSelectorManager.fillColor = [UIColor ArounMeColor];
    //
    self.mapSelectorManager.strokeColor = [UIColor ArounMeColor];
    _placeName.text = _roomName;
    // Do any additional setup after loading the view.
}
- (void)viewWillAppear:(BOOL)animated {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"Circled Chevron Left -50"]
                                             style:UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(onBack:)];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
}

- (void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    //    MKPinAnnotationView *annView=[[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:@"pin"];
    //    annView.pinColor = MKPinAnnotationColorRed;
    //    return annView;
    return [self.mapSelectorManager mapView:mapView viewForAnnotation:annotation];
}
#pragma mark - DBMapSelectorManager Delegate

//a
- (void)mapSelectorManager:(DBMapSelectorManager *)mapSelectorManager didChangeCoordinate:(CLLocationCoordinate2D)coordinate {
    NSLog(@"lat %f lon %f", coordinate.latitude, coordinate.longitude);
    latitude = coordinate.latitude;
    longitude = coordinate.longitude;
    //    _coordinateLabel.text = [NSString stringWithFormat:@"Coordinate = {%.5f, %.5f}", coordinate.latitude, coordinate.longitude];
}

- (void)mapSelectorManager:(DBMapSelectorManager *)mapSelectorManager didChangeRadius:(CLLocationDistance)radius {
    Radius = (radius >= 1000) ?  radius * .001f : radius;
    NSLog(@"Radius %f", Radius);
}

//- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
//    return [self.mapSelectorManager mapView:mapView viewForAnnotation:annotation];
//}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)annotationView didChangeDragState:(MKAnnotationViewDragState)newState fromOldState:(MKAnnotationViewDragState)oldState {
    [self.mapSelectorManager mapView:mapView annotationView:annotationView didChangeDragState:newState fromOldState:oldState];
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id <MKOverlay>)overlay {
    return [self.mapSelectorManager mapView:mapView rendererForOverlay:overlay];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
    [self.mapSelectorManager mapView:mapView regionDidChangeAnimated:animated];
}
- (void)startLocationReporting {
    NSLog(@"startLocation");
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;//or whatever class you have for managing location
    _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    [_locationManager startUpdatingLocation];
    //    [_locationManager startMonitoringSignificantLocationChanges];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    NSLog(@"locationManager");
    latUser = newLocation.coordinate.latitude;
    lonUser= newLocation.coordinate.longitude;
    NSLog(@"coord %f %f", latUser, lonUser);
    //    [_locationManager stopUpdatingLocation];
}

- (IBAction)completeAction:(id)sender {
    if (![_placeName.text isEqualToString:@""]){
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"title": _placeName.text, @"radius": @(Radius), @"longitude":@(longitude), @"latitude":@(latitude), @"user_id":[NSUserDefaults userID], @"room_id":_roomID};
        [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIeditroom] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            if ([[responseObject valueForKey:@"status"] isEqualToString:@"success"]) {
                [self showMessage:nil title:NSLocalizedString(@"Edit successful", nil) buttonCount:1 action2:@"back"];
            }
            else {
                [self showMessage:NSLocalizedString(@"Try again later.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
            }
            NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [self showMessage:NSLocalizedString(@"Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
            
            NSLog(@"Error: %@", error);
        }];
    }
    else {
        [self showMessage:NSLocalizedString(@"Fill in all fields.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
    }
}


@end
