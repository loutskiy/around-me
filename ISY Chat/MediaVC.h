//
//  MediaVC.h
//  Around me
//
//  Created by Михаил Луцкий on 25.10.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JMImageCache/JMImageCache.h>
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "Constants.h"
#import "UserCache.h"
#import "UIAlertMessage.h"

@interface MediaVC : UIViewController <UIActionSheetDelegate, MBProgressHUDDelegate>

@property (strong, nonatomic) NSString *roomID;
@property (strong, nonatomic) NSString *messageID;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSString *data;

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

- (IBAction)actions:(id)sender;

@end
