//
//  UsersViewCell.h
//  Around me
//
//  Created by Михаил Луцкий on 03.10.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UsersViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
@property (weak, nonatomic) IBOutlet UILabel *userLogin;

@end
