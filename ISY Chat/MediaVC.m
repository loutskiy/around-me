//
//  MediaVC.m
//  Around me
//
//  Created by Михаил Луцкий on 25.10.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "MediaVC.h"

@interface MediaVC () {
    MBProgressHUD *HUD;
}

@end

@implementation MediaVC

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"\nData: %@\nType: %@\nMessage ID: %@\nRoom ID: %@\n",_data,_type,_messageID,_roomID);
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"Circled Chevron Left -50"]
                                             style:UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(onBack:)];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    if ([_type isEqualToString:@"Photo"]) {
        [_imageView setImageWithURL:[NSURL URLWithString:_data] key:nil placeholder:nil completionBlock:nil failureBlock:nil];
    }
    else {
        NSString *aString = _data;
        NSArray *arrayLOC = [aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        arrayLOC = [arrayLOC filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
        [_imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%@,%@&zoom=16&size=%0dx%0.f&scale=2&sensor=true&markers=%@,%@", arrayLOC[0], arrayLOC[1],320,self.imageView.frame.size.height, arrayLOC[0], arrayLOC[1]]] key:nil placeholder:nil completionBlock:nil failureBlock:nil];

    }
    // Do any additional setup after loading the view.
}
- (void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)actions:(id)sender {
    UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Actions", nil)
                                                          delegate:self
                                                 cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                            destructiveButtonTitle:NSLocalizedString(@"Delete message", nil)
                                                 otherButtonTitles:nil];
    [actSheet showInView:self.view];
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
        NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"user_id":[NSUserDefaults userID], @"message_id":_messageID, @"room_id":_roomID};
        [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIdeletemessage] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
            HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
            [self.navigationController.view addSubview:HUD];

            HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Delete-100"]];
            
            // Set custom view mode
            HUD.mode = MBProgressHUDModeCustomView;
            
            HUD.delegate = self;
            HUD.labelText = NSLocalizedString(@"Deleted", nil);
            
            [HUD show:YES];
            [HUD hide:YES afterDelay:2];
            [self.navigationController popViewControllerAnimated:YES];
            //        [self.demoData.messages removeObjectAtIndex:indexPath.item];
            //        [collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];
            NSLog(@"JSON: %@", responseObject);
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"Error: %@", error);
        }];
    }
}
@end
