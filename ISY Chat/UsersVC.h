//
//  UsersVC.h
//  Around me
//
//  Created by Михаил Луцкий on 02.10.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
#import <JMImageCache/JMImageCache.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "Constants.h"
#import "UserCache.h"
#import "UsersViewCell.h"
#import "UIAlertMessage.h"
#import "UIColor+ISYColors.h"

@interface UsersVC : UICollectionViewController
@property (strong, nonatomic) NSString *roomID;

@end
