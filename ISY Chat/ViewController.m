//
//  ViewController.m
//  ISY Chat
//
//  Created by Михаил Луцкий on 17.08.15.
//  Copyright (c) 2015 LWTS Technologies. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "UserCache.h"
#import "AuthVC.h"
#import "FindVC.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewDidAppear:(BOOL)animated {
    if ([NSUserDefaults isAuth]) {
        NSLog(@"\n*******************************************\nWELCOME TO Around me\nDeveloper: Loutskiy Mikhail\n2015 (с) Copyright Around me. All rights reserved.\nToken: %@\nUser_ID: %@\n*******************************************\n", [NSUserDefaults userToken], [NSUserDefaults userID]);
        FindVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainTabBar"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else {
        AuthVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AuthVC"];
        [self presentViewController:vc animated:NO completion:nil];
    }
}

@end
