//
//  AppDelegate.m
//  ISY Chat
//
//  Created by Михаил Луцкий on 17.08.15.
//  Copyright (c) 2015 LWTS Technologies. All rights reserved.
//

#import "AppDelegate.h"
#import <AFNetworking/AFNetworkActivityIndicatorManager.h>
#import <Parse/Parse.h>
#import <GoogleMaps/GoogleMaps.h>
#import "UIColor+ISYColors.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [Parse setApplicationId:@"bgblSiKLo68Ylg6mfZtYOqnkCuXHu1hVDRztFvn9"
                  clientKey:@"JYJ3f8SASU5iADlHcEWGP4ZRwJBCYGNCjACn4tjO"];
    [GMSServices provideAPIKey:@"AIzaSyDX_RFHfJlm4qyp2CVQou60wbJ8NoWR6QM"];
    [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
    [[UITabBar appearance] setTintColor:[UIColor ArounMeColor]];
    // Register for Push Notitications
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [application registerUserNotificationSettings:settings];
    [application registerForRemoteNotifications];

    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    [PFPush handlePush:userInfo];
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

@end
