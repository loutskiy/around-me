//
//  MapVC.m
//  ISY Chat
//
//  Created by Михаил Луцкий on 20.09.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "MapVC.h"
#import <AFNetworking/AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "UserCache.h"
#import "Constants.h"
#import "UIAlertMessage.h"
#import "AnnotationPins.h"
#import "MessagesViewController.h"

// We need a custom subclass of MKMapView in order to allow touches on UIControls in our custom callout view.
@interface CustomMapView : MKMapView
@property (nonatomic, strong) SMCalloutView *calloutView;
@end

@interface MapVC () <CLLocationManagerDelegate>{
    float latUser, lonUser;
    BOOL internetConnection;
    NSArray *rooms;
}
@property (nonatomic,retain) CLLocationManager *locationManager;

@end

@implementation MapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self startLocationReporting];
    [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    // Do any additional setup after loading the view.
    
//        self.annotationForUICalloutView = [MKPointAnnotation new];
//        self.annotationForUICalloutView.coordinate = (CLLocationCoordinate2D){28.388154, -80.604200};
//        self.annotationForUICalloutView.title = @"Cape Canaveral";
//        self.annotationForUICalloutView.subtitle = @"Launchpad";
//        self.mapView = [[MKMapView alloc] init];
        self.mapView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.mapView.delegate = self;
        [self.mapView addAnnotation:self.annotationForUICalloutView];
        [self.view addSubview:self.mapView];
    
    // create our custom callout view
    self.calloutView = [SMCalloutView platformCalloutView];
    self.calloutView.delegate = self;
    
    // tell our custom map view about the callout so it can send it touches
//    self.mapKitWithSMCalloutView.calloutView = self.calloutView;
}

- (void) viewWillAppear:(BOOL)animated {
    internetConnection = true;
    [self loadData:@"FirstLoad"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_locationManager stopUpdatingLocation];
}

- (void) loadData:(NSString*)type {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *request = [NSString stringWithFormat:@"%@%@?token=%@&user_id=%@&latitude=%f&longitude=%f&offset=%i&limit=%i", kBaseAPIurl,kAPIallrooms, [NSUserDefaults userToken],[NSUserDefaults userID], latUser, lonUser, 0, 100];
    NSLog(@"request %@",request);
    [manager GET:request parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        internetConnection = true;
        if ([[responseObject valueForKey:@"status"] isEqualToString:@"success"]){
            rooms = [responseObject valueForKey:@"response"];
            [self addPinsToMap];
        }
        else {
            rooms = nil;
            [self showMessage:NSLocalizedString(@"Nothing Found. Try again later or create a new conversetion.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
        }
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (internetConnection){
            internetConnection = false;
            [self showMessage:NSLocalizedString(@"Cannot update rooms. Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
        }
        NSLog(@"Error: %@", error);
    }];
}

- (void) addPinsToMap {
    for (int i = 0; i < rooms.count; i++) {
        NSArray  *roomData = rooms[i];
        NSNumber *latitude = [roomData valueForKey:@"latitude"];
        NSNumber *longitude = [roomData valueForKey:@"longitude"];
        NSString *title = [roomData valueForKey:@"title"];
        //Create coordinates from the latitude and longitude values
        CLLocationCoordinate2D coord;
        coord.latitude = latitude.doubleValue;
        coord.longitude = longitude.doubleValue;
        MyAnnotation *annotation = [[MyAnnotation alloc] init];
        annotation.roomID = [NSString stringWithFormat:@"%d", i];
        [annotation setCoordinate:coord];
        [annotation setSubtitle:[NSString stringWithFormat:@"%@: %@", NSLocalizedString(@"Destination", nil), [self metersConverter:[[roomData valueForKey:@"meters"] floatValue]]]];
        [annotation setTitle:title]; //You can set the subtitle too
        [self.mapView addAnnotation:annotation];
    }
}

- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    // create a proper annotation view, be lazy and don't use the reuse identifier
    MKPinAnnotationView *view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@""];
//    view.annotation.title;
    // create a disclosure button for map kit
    UIButton *disclosure = [UIButton buttonWithType:UIButtonTypeCustom];
    [disclosure setFrame:CGRectMake(0, 0, 25, 25)];
    [disclosure setBackgroundImage:[UIImage imageNamed:@"Circled Right 2-50"] forState:UIControlStateNormal];
    disclosure.tag = [((MyAnnotation *) annotation).roomID integerValue];
    [disclosure addTarget:self action:@selector(disclosureTapped:) forControlEvents:UIControlEventTouchUpInside];
    view.rightCalloutAccessoryView = disclosure;
    
    // if we're using SMCalloutView, we don't want MKMapView to create its own callout!
    if (annotation == self.annotationForSMCalloutView)
        view.canShowCallout = NO;
    else
        view.canShowCallout = YES;
    
    return view;
}
- (void)disclosureTapped: (id) sender {
    UIButton *clicked = (UIButton *) sender;
    MessagesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
    NSArray *roomData = rooms[clicked.tag];
    vc.roomID = [roomData valueForKey:@"room_id"];
    vc.Latitude = [roomData valueForKey:@"latitude"];
    vc.Longitude = [roomData valueForKey:@"longitude"];
    vc.roomName = [roomData valueForKey:@"title"];
    vc.inFavs = [roomData valueForKey:@"inFavs"];
    vc.isAdmin = [roomData valueForKey:@"isAdmin"];
    vc.Radius = [roomData valueForKey:@"radius"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}
//- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
//    
//    [self.calloutView dismissCalloutAnimated:YES];
//}

- (void)startLocationReporting {
    NSLog(@"startLocation");
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;//or whatever class you have for managing location
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    _locationManager.distanceFilter =  kCLDistanceFilterNone;
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    }else{
        [self.locationManager startUpdatingLocation];
    }//    [_locationManager startMonitoringSignificantLocationChanges];
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [self.locationManager startUpdatingLocation];
        }
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //    NSLog(@"locationManager");
    if (!(oldLocation.coordinate.latitude == newLocation.coordinate.latitude && oldLocation.coordinate.longitude == newLocation.coordinate.longitude)) {
        latUser = newLocation.coordinate.latitude;
        lonUser= newLocation.coordinate.longitude;
        NSLog(@"coord %f %f", latUser, lonUser);
        [self loadData:@"FirstLoad"];
    }
    
    //    [_locationManager stopUpdatingLocation];
}

- (IBAction)getUserLocation:(id)sender {
    [_mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
}

- (NSString*) metersConverter: (float) metersValue {
    NSString *response;
    if (metersValue >= 1000) {
        metersValue /= 1000;
        response = [NSString stringWithFormat:@"%.f %@",metersValue, NSLocalizedString(@"km", nil)];
    }
    else {
        response = [NSString stringWithFormat:@"%.f %@",metersValue, NSLocalizedString(@"m", nil)];
    }
    return response;
}

- (IBAction)refreshMap:(id)sender {
    [self removeAllPinsButUserLocation1];
    [self loadData:@"FirstLoad"];
}

- (void)removeAllPinsButUserLocation1
{
    id userLocation = [_mapView userLocation];
    [_mapView removeAnnotations:[_mapView annotations]];
    
    if ( userLocation != nil ) {
        [_mapView addAnnotation:userLocation]; // will cause user location pin to blink
    }
}
@end
//
// Custom Map View
//
// We need to subclass MKMapView in order to present an SMCalloutView that contains interactive
// elements.
//

@interface MKMapView (UIGestureRecognizer)

// this tells the compiler that MKMapView actually implements this method
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch;

@end

@implementation CustomMapView

// override UIGestureRecognizer's delegate method so we can prevent MKMapView's recognizer from firing
// when we interact with UIControl subclasses inside our callout view.
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isKindOfClass:[UIControl class]])
        return NO;
    else
        return [super gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
}

// Allow touches to be sent to our calloutview.
// See this for some discussion of why we need to override this: https://github.com/nfarina/calloutview/pull/9
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    
    UIView *calloutMaybe = [self.calloutView hitTest:[self.calloutView convertPoint:point fromView:self] withEvent:event];
    if (calloutMaybe) return calloutMaybe;
    
    return [super hitTest:point withEvent:event];
}

@end

