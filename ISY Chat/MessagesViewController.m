#import "MessagesViewController.h"

@implementation MessagesViewController  {
    SRWebSocket *rusSocket;
    int Offset, Limit;
    MBProgressHUD *HUD;
    NSMutableArray *avatarsIMGs;
    NSMutableArray *messages;
    NSMutableArray *types;
    NSMutableArray *messagesID;
    NSMutableArray *usersID;
    float latUser, lonUser;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    Offset = 0;
    Limit = 25;
//    isUserSentMess = false;
    [self loadData];
    [self setupSockets];
    self.title = _roomName;

    /**
     *  You MUST set your senderId and display name
     */
    self.senderId = [NSUserDefaults userID];
    self.senderDisplayName = [NSUserDefaults userID];
    
    if ([_roomID isEqualToString:@"1"] && ![[NSUserDefaults userType] isEqualToString:@"admin"]){
        self.inputToolbar.hidden = YES;
    }
    if (![_roomID isEqualToString:@"1"]){
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Circled Down-50"]
                                                                                  style:UIBarButtonItemStyleDone
                                                                                 target:self
                                                                                 action:@selector(presentMenuButtonTapped:)];
    }
//    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
//    [UIMenuController sharedMenuController].menuItems = @[ [[UIMenuItem alloc] initWithTitle:@"Custom Action"
//                                                                                      action:@selector(customAction:)] ];
//    if ([[NSUserDefaults userType] isEqualToString:@"admin"]) {
        [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
//    }
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    
    /**
     *  Load up our fake data for the demo
     */
    self.demoData = [[ModelData alloc] init];
    avatarsIMGs = [[NSMutableArray alloc] init];
    messages = [[NSMutableArray alloc] init];
    messagesID = [[NSMutableArray alloc] init];
    usersID = [[NSMutableArray alloc] init];
    types = [[NSMutableArray alloc] init];
    self.showLoadEarlierMessagesHeader = YES;

    [self initiateMenuOptions];
}

- (void) loadData {
    [self showProgressHUD];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"user_id": [NSUserDefaults userID], @"room_id": _roomID, @"offset": @(Offset), @"limit": @(Limit)};
    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIgetmessages] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        if ([[responseObject valueForKey:@"status"] isEqualToString:@"failed"] && ![[responseObject valueForKey:@"response"] isEqualToString:@"No messages"]) {
//            if (![[responseObject valueForKey:@"response"] isEqualToString:@"No messages"]){
                self.inputToolbar.hidden = YES;
                self.showLoadEarlierMessagesHeader = NO;
                [HUD hide:YES];
                [self showMessage:NSLocalizedString(@"Sorry, you are banned in this chat.", nil) title:@"Error" buttonCount:1];
                [rusSocket close];
//            }
        }
        else {
            self.inputToolbar.hidden = NO;
            self.showLoadEarlierMessagesHeader = YES;
            NSArray *messagesData = [responseObject valueForKey:@"messages"];
            for (int i = 0; i < messagesData.count; i++) {
                [avatarsIMGs addObject:[messagesData[i] valueForKey:@"avatar"]];
                NSTimeInterval interval = [[messagesData[i] valueForKey:@"unix_time"] doubleValue];
                JSQMessage*message;
                if ([[messagesData[i] valueForKey:@"type"] isEqualToString:@"Text"]){
                    message = [[JSQMessage alloc] initWithSenderId:[messagesData[i] valueForKey:@"user_id"]
                                   senderDisplayName:[messagesData[i] valueForKey:@"login"]
                                                                     date:[NSDate dateWithTimeIntervalSince1970:interval]
                                                text:[messagesData[i] valueForKey:@"data"]];
                }
                else if ([[messagesData[i] valueForKey:@"type"] isEqualToString:@"Photo"]){
                    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@""]];
                    
                    message = [[JSQMessage alloc] initWithSenderId:[messagesData[i] valueForKey:@"user_id"] senderDisplayName:[messagesData[i] valueForKey:@"login"] date:[NSDate dateWithTimeIntervalSince1970:interval] media:photoItem];
                    
                }
                else if ([[messagesData[i] valueForKey:@"type"] isEqualToString:@"Location"]){
                    NSString *aString = [messagesData[i] valueForKey:@"data"];
                    NSArray *arrayLOC = [aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    arrayLOC = [arrayLOC filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
                    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:[arrayLOC[0] doubleValue] longitude:[arrayLOC[1] doubleValue]];
                    
                    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
                    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{ [self.collectionView reloadData]; } ];
                    message = [[JSQMessage alloc] initWithSenderId:[messagesData[i] valueForKey:@"user_id"] senderDisplayName:[messagesData[i] valueForKey:@"login"] date:[NSDate dateWithTimeIntervalSince1970:interval] media:locationItem];
                    
                }
                [messagesID addObject:[messagesData[i] valueForKey:@"message_id"]];
                [usersID addObject:[messagesData[i] valueForKey:@"user_id"]];
                [types addObject:[messagesData[i] valueForKey:@"type"]];
                [messages addObject:[messagesData[i] valueForKey:@"data"]];
                [self.demoData.messages addObject:message];
            }
        [self.collectionView reloadData];
        [self scrollToBottomAnimated:YES];
        [HUD hide:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HUD hide:YES];
        [self showMessage:NSLocalizedString(@"Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
        NSLog(@"Error: %@", error);
    }];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation{
    //should be called after rotation animation completed
    [self.contextMenuTableView reloadData];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
    
    [self.contextMenuTableView updateAlongsideRotation];
}

- (void)viewWillTransitionToSize:(CGSize)size
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    
    [coordinator animateAlongsideTransition:nil completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
        //should be called after rotation animation completed
        [self.contextMenuTableView reloadData];
    }];
    [self.contextMenuTableView updateAlongsideRotation];
    
}

#pragma mark - IBAction

- (IBAction)presentMenuButtonTapped:(UIBarButtonItem *)sender {
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    // init YALContextMenuTableView tableView
    if (!self.contextMenuTableView) {
        self.contextMenuTableView = [[YALContextMenuTableView alloc]initWithTableViewDelegateDataSource:self];
        self.contextMenuTableView.animationDuration = 0.15;
        //optional - implement custom YALContextMenuTableView custom protocol
        self.contextMenuTableView.yalDelegate = self;
        
        //register nib
        UINib *cellNib = [UINib nibWithNibName:@"ContextMenuCell" bundle:nil];
        [self.contextMenuTableView registerNib:cellNib forCellReuseIdentifier:menuCellIdentifier];
    }
    
    // it is better to use this method only for proper animation
    [self.contextMenuTableView showInView:self.navigationController.view withEdgeInsets:UIEdgeInsetsZero animated:YES];
}
- (void)showPopupWithTransitionStyle:(STPopupTransitionStyle)transitionStyle rootViewController:(UIViewController *)rootViewController
{
    STPopupController *popupController = [[STPopupController alloc] initWithRootViewController:rootViewController];
    popupController.cornerRadius = 4;
    popupController.transitionStyle = transitionStyle;
    [popupController presentInViewController:self];
}
#pragma mark - Local methods

- (void)initiateMenuOptions {
    NSString *favsString, *favsIMG;
    NSLog(@"inFavs %@", _inFavs);
    if ([_inFavs isEqualToString:@"1"]){
        favsString = NSLocalizedString(@"Delete from favourites", nil);
        favsIMG = @"Star Filled-64";
    }
    else {
        favsString = NSLocalizedString(@"Add to favourites", nil);
        favsIMG = @"Star-64";
    }
    if ([_isAdmin isEqualToString:@"1"] || [[NSUserDefaults userID] isEqualToString:@"1"]){
        self.menuTitles = @[@"",
                            favsString,
                            NSLocalizedString(@"Edit", nil),
                            NSLocalizedString(@"People", nil),
                            NSLocalizedString(@"Info", nil),
                            NSLocalizedString(@"Complain", nil),
                            NSLocalizedString(@"Delete", nil)];
        
        self.menuIcons = @[[UIImage imageNamed:@"Circled Up 2-64"],
                           [UIImage imageNamed:favsIMG],
                           [UIImage imageNamed:@"Edit-64"],
                           [UIImage imageNamed:@"Conference-64"],
                           [UIImage imageNamed:@"Info-64"],
                           [UIImage imageNamed:@"Error-64"],
                           [UIImage imageNamed:@"Trash-64"]];
    }
    else {
        self.menuTitles = @[@"",
                            favsString,
                            NSLocalizedString(@"People", nil),
                            NSLocalizedString(@"Info", nil),
                            NSLocalizedString(@"Complain", nil)];
        
        self.menuIcons = @[[UIImage imageNamed:@"Circled Up 2-64"],
                           [UIImage imageNamed:favsIMG],
                           [UIImage imageNamed:@"Conference-64"],
                           [UIImage imageNamed:@"Info-64"],
                           [UIImage imageNamed:@"Error-64"]];
    }
}


#pragma mark - YALContextMenuTableViewDelegate

- (void)contextMenuTableView:(YALContextMenuTableView *)contextMenuTableView didDismissWithIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Menu dismissed with indexpath = %@", indexPath);
    if (indexPath.row == 1) [self favouriteAction];
    if (indexPath.row == 6) [self deleteChat];
    if ([_isAdmin isEqualToString:@"1"]){
        if (indexPath.row == 2) [self editRoomOpen];
        if (indexPath.row == 3) [self usersOpen];
        if (indexPath.row == 4) [self infoOpen];
        if (indexPath.row == 5) [self mailOpen];
    }
    else {
        if (indexPath.row == 2) [self usersOpen];
        if (indexPath.row == 3) [self infoOpen];
        if (indexPath.row == 4) [self mailOpen];
    }
}

- (void) infoOpen {
    [STPopupNavigationBar appearance].barTintColor = [UIColor ArounMeColor];
    [STPopupNavigationBar appearance].tintColor = [UIColor whiteColor];
    [STPopupNavigationBar appearance].barStyle = UIBarStyleDefault;

    NSDictionary *chatInfo = @{@"Room_id":_roomID, @"Title":_roomName, @"Latitude":_Latitude, @"Longitude":_Longitude};
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChatInfo" object:@"Hello Notification"];
    [self showPopupWithTransitionStyle:STPopupTransitionStyleSlideVertical rootViewController:[self.storyboard instantiateViewControllerWithIdentifier:@"InfoChat"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ChatInfo" object:chatInfo];
}

- (void) editRoomOpen {
    EditRoomVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditRoomVC"];
    vc.roomID = _roomID;
    vc.Latitude = _Latitude;
    vc.Longitude = _Longitude;
    vc.radius = _Radius;
    vc.roomName = _roomName;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) usersOpen {
    UsersVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"UsersVC"];
    vc.roomID = _roomID;
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) mailOpen {
    NSString *emailTitle = NSLocalizedString(@"Report message from Around me", nil);
    NSString *messageBody = [NSString stringWithFormat:@"\nRoom_ID: %@ %@", _roomID, NSLocalizedString(@"\n\nSend from Around me app for iOS", nil)];
    NSArray *toRecipents = [NSArray arrayWithObject:@"support@lwts.ru"];
    MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
    mc.mailComposeDelegate = self;
    [mc setSubject:emailTitle];
    [mc setMessageBody:messageBody isHTML:NO];
    [mc setToRecipients:toRecipents];
    
    [self presentViewController:mc animated:YES completion:NULL];
}

- (void) deleteChat {
    [self showProgressHUD];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"user_id": [NSUserDefaults userID], @"room_id":_roomID};
    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIdeleteroom] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [HUD hide:YES];
        [self.navigationController popViewControllerAnimated:YES];
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

- (void) completeHUD:(NSString*) typeHUD {
    HUD = [[MBProgressHUD alloc] initWithView:self.navigationController.view];
    [self.navigationController.view addSubview:HUD];
    if ([typeHUD isEqualToString:NSLocalizedString(@"Added", nil)]){
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Checkmark-100"]];
    }
    else {
        HUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Delete-100"]];
    }
    
    // Set custom view mode
    HUD.mode = MBProgressHUDModeCustomView;
    
    HUD.delegate = self;
    HUD.labelText = typeHUD;
    
    [HUD show:YES];
    [HUD hide:YES afterDelay:2];
}

- (void) showProgressHUD {
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    HUD.dimBackground = NO;
    HUD.color = [UIColor ArounMeColor];
    HUD.delegate = self;
    [HUD show:YES];
}

- (void) favouriteAction {
    [self showProgressHUD];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@?token=%@&user_id=%@&room_id=%@", kBaseAPIurl, kAPIfavs, [NSUserDefaults userToken], [NSUserDefaults userID], _roomID] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [HUD hide:YES];
        if ([[responseObject valueForKey:@"status"] isEqualToString:@"success"]) {
            if ([[responseObject valueForKey:@"response"] isEqualToString:@"added"]) {
                [self completeHUD:NSLocalizedString(@"Added", nil)];
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation addUniqueObject:[NSString stringWithFormat:@"chat%@",_roomID] forKey:@"channels"];
                [currentInstallation saveInBackground];
                _inFavs = @"1";
            }
            else if ([[responseObject valueForKey:@"response"] isEqualToString:@"deleted"]) {
                [self completeHUD:NSLocalizedString(@"Deleted", nil)];
                PFInstallation *currentInstallation = [PFInstallation currentInstallation];
                [currentInstallation removeObject:[NSString stringWithFormat:@"chat%@",_roomID] forKey:@"channels"];
                [currentInstallation saveInBackground];
                _inFavs = @"0";
            }
        }
        [self initiateMenuOptions];
        [self.contextMenuTableView reloadData];
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HUD hide:YES];

        NSLog(@"Error: %@", error);
    }];
}

#pragma mark - UITableViewDataSource, UITableViewDelegate

- (void)tableView:(YALContextMenuTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView dismisWithIndexPath:indexPath];
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 65;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuTitles.count;
}

- (UITableViewCell *)tableView:(YALContextMenuTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContextMenuCell *cell = [tableView dequeueReusableCellWithIdentifier:menuCellIdentifier forIndexPath:indexPath];
    
    if (cell) {
        cell.backgroundColor = [UIColor clearColor];
        cell.menuTitleLabel.text = [self.menuTitles objectAtIndex:indexPath.row];
        cell.menuImageView.image = [self.menuIcons objectAtIndex:indexPath.row];
    }
    
    return cell;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self startLocationReporting];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"Circled Chevron Left -50"]
                                             style:UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(onBack:)];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
    self.collectionView.collectionViewLayout.springinessEnabled = NO;

}
-(void)viewWillDisappear:(BOOL)animated {
    [_locationManager stopUpdatingLocation];
    
}
- (void)onBack:(id)sender
{
    [rusSocket close];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)startLocationReporting {
    NSLog(@"startLocation");
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;//or whatever class you have for managing location
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    _locationManager.distanceFilter =  kCLDistanceFilterNone;
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    }else{
        [self.locationManager startUpdatingLocation];
    }//    [_locationManager startMonitoringSignificantLocationChanges];
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [self.locationManager startUpdatingLocation];
        }
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //    NSLog(@"locationManager");
    if (!(oldLocation.coordinate.latitude == newLocation.coordinate.latitude && oldLocation.coordinate.longitude == newLocation.coordinate.longitude)) {
        latUser = newLocation.coordinate.latitude;
        lonUser= newLocation.coordinate.longitude;
        NSLog(@"coord %f %f", latUser, lonUser);
    }
}

#pragma mark - Testing

- (void)setupSockets
{
    NSString *adress = [NSString stringWithFormat:@"%@chat?room_id=%@", kBaseAPIurl, _roomID];
    NSLog(@"Request adress %@", adress);
    NSURL *url = [NSURL URLWithString:adress];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    rusSocket = [[SRWebSocket alloc] initWithURLRequest:request];
    rusSocket.delegate = self;
    [rusSocket open];
    
}

-(void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error {
    [rusSocket close];
    rusSocket = nil;
    [self setupSockets];
    NSLog(@"Error");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    NSData *data = [message dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    [avatarsIMGs addObject:[json valueForKey:@"avatar"]];
    NSLog(@"message %@", message);

    NSTimeInterval interval = [[json valueForKey:@"unix_time"] doubleValue];

    JSQMessage*messageRecive;
    if ([[json valueForKey:@"type"] isEqualToString:@"Text"]){
        messageRecive = [[JSQMessage alloc] initWithSenderId:[json valueForKey:@"user_id"]
                                     senderDisplayName:[json valueForKey:@"login"]
                                                  date:[NSDate dateWithTimeIntervalSince1970:interval]
                                                  text:[json valueForKey:@"data"]];
    }
    else if ([[json valueForKey:@"type"] isEqualToString:@"Photo"]){
        JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@""]];
        messageRecive = [[JSQMessage alloc] initWithSenderId:[json valueForKey:@"user_id"] senderDisplayName:[json valueForKey:@"login"] date:[NSDate dateWithTimeIntervalSince1970:interval] media:photoItem];
        
    }
    else if ([[json valueForKey:@"type"] isEqualToString:@"Location"]){
        NSString *aString = [json valueForKey:@"data"];
        NSArray *arrayLOC = [aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        arrayLOC = [arrayLOC filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
        CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:[arrayLOC[0] doubleValue] longitude:[arrayLOC[1] doubleValue]];
        
        JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
        [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{ [self.collectionView reloadData]; } ];
        messageRecive = [[JSQMessage alloc] initWithSenderId:[json valueForKey:@"user_id"] senderDisplayName:[json valueForKey:@"login"] date:[NSDate dateWithTimeIntervalSince1970:interval] media:locationItem];
        
    }
    Offset++;
    [types addObject:[json valueForKey:@"type"]];
    [messagesID addObject:[json valueForKey:@"message_id"]];
    [messages addObject:[json valueForKey:@"data"]];
    [usersID addObject:[json valueForKey:@"user_id"]];
    [self.demoData.messages addObject:messageRecive];
//    [self.demoData.avatars addObject:avatar];
    [self.collectionView reloadData];
    if ([[json valueForKey:@"user_id"] isEqualToString:[NSUserDefaults userID]]) {
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        [self scrollToBottomAnimated:YES];
        [self finishSendingMessageAnimated:YES];
    }
    else {
        [self scrollToBottomAnimated:YES];
        [self finishSendingMessageAnimated:YES];

        [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
    }
    NSLog(@"message %@", [json valueForKey:@"data"]);
}


#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    [self finishSendingMessageAnimated:YES];
    NSString *formatedString = [text stringByReplacingOccurrencesOfString:@"\\" withString:@"\\\\"];
    formatedString = [formatedString stringByReplacingOccurrencesOfString:@"\"" withString:@"\\\""];
    NSLog(@"formated %@", formatedString);
    NSString *helloMsg = [NSString stringWithFormat:@"{\"user_id\":\"%@\",\"room_id\":\"%@\",\"data\":\"%@\", \"token\":\"%@\", \"type\":\"Text\"}", [NSUserDefaults userID], _roomID, formatedString, [NSUserDefaults userToken]];
    NSLog(@"ff %@", helloMsg);
    [rusSocket send:helloMsg];
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Media messages", nil)
                                                       delegate:self
                                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)                                          destructiveButtonTitle:nil
                                              otherButtonTitles:NSLocalizedString(@"Send photo", nil), NSLocalizedString(@"Send location", nil), nil];
    sheet.tag = 1;
    [self.inputToolbar.contentView.textView resignFirstResponder];
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    if (actionSheet.tag == 1){
        switch (buttonIndex) {
            case 0:{
                PKImagePickerViewController *vc=[[PKImagePickerViewController alloc]init];
                vc.roomID = _roomID;
                vc.typePhoto = @"chat";
                [self.navigationController presentViewController:vc animated:YES completion:nil];
            }
                break;
                
            case 1:
            {
                NSString *helloMsg = [NSString stringWithFormat:@"{\"User_id\":\"%@\",\"Room_id\":\"%@\",\"Data\":\"%f %f\", \"Token\":\"%@\", \"Type\":\"Location\"}", [NSUserDefaults userID], _roomID, latUser, lonUser, [NSUserDefaults userToken]];
                [rusSocket send:helloMsg];
            }
                break;
    //
    //        case 2:
    //            [self.demoData addVideoMediaMessage];
    //            break;
        }
    }
    else {
        switch (buttonIndex) {
            case 0:{
                if (![[NSString stringWithFormat:@"%lu", actionSheet.tag] isEqualToString:[NSUserDefaults userID]]) {
                    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
                    NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"user_id": [NSUserDefaults userID], @"user_for_ban":@(actionSheet.tag), @"room_id":_roomID};
                    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIbanuser] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                        [self showMessage:NSLocalizedString(@"User is banned", nil) title:@"Success" buttonCount:1];
                        NSLog(@"JSON: %@", responseObject);
                    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                        [self showMessage:NSLocalizedString(@"Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];

                        NSLog(@"Error: %@", error);
                    }];
                }
                else {
                    [self showMessage:NSLocalizedString(@"You can not ban yourself", nil) title:@"Error" buttonCount:1];
                }
            }
                break;
            case 1:{
                [self mailOpen];
            }
                break;
        }
//        NSLog(@"Button index %i", buttonIndex);
    }
//    [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
//    [self finishSendingMessageAnimated:YES];
}



#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.demoData.messages objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"user_id":[NSUserDefaults userID], @"message_id":messagesID[indexPath.row], @"room_id":_roomID};
    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIdeletemessage] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [messagesID removeObjectAtIndex:indexPath.row];
        [messages removeObjectAtIndex:indexPath.row];
        [avatarsIMGs removeObjectAtIndex:indexPath.row];
        [types removeObjectAtIndex:indexPath.row];
        [usersID removeObjectAtIndex:indexPath.row];
//        [self.demoData.messages removeObjectAtIndex:indexPath.item];
//        [collectionView deleteItemsAtIndexPaths:[NSArray arrayWithObject:indexPath]];

        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    [self.demoData.messages removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.demoData.outgoingBubbleImageData;
    }
    return self.demoData.incomingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
        return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    }
    
    return nil;
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.demoData.messages count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    [cell.avatarImageView setImageWithURL:[NSURL URLWithString:avatarsIMGs[indexPath.row]] key:nil placeholder:[UIImage imageNamed:@"profile"] completionBlock:nil failureBlock:nil];
    cell.avatarContainerView.layer.cornerRadius = cell.avatarContainerView.frame.size.width / 2.0;
    cell.avatarImageView.contentMode = UIViewContentModeScaleAspectFill;
    cell.avatarContainerView.clipsToBounds = YES;
    cell.mediaView.layer.cornerRadius = 20;
    cell.mediaView.clipsToBounds = YES;
    cell.mediaView.layer.borderWidth = 5;
    cell.mediaView.layer.borderColor = [[UIColor clearColor] CGColor];
//    cell.cellBottomLabel.text = @"123";
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            cell.textView.textColor = [UIColor blackColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    else {
//        if ([types[indexPath.row] isEqualToString:@"Photo"]){
            UIImageView *imageView = [[UIImageView alloc] init];
//            if (![msg.senderId isEqualToString:self.senderId] && [types[indexPath.row] isEqualToString:@"Photo"]) {
//                CGAffineTransform horizontalFlip = CGAffineTransformMakeScale(-1,1);
//                CGAffineTransform horizontalFlip2 = CGAffineTransformMakeScale(-1,1);
//                cell.mediaView.transform = horizontalFlip;
//                imageView.transform = horizontalFlip2;
//            }
            [imageView setImageWithURL:[NSURL URLWithString:messages[indexPath.row]] key:nil placeholder:nil completionBlock:nil failureBlock:nil];
//            imageView.image = [UIImage imageNamed:@"Ok-64"];
            imageView.clipsToBounds = YES;

            imageView.contentMode = UIViewContentModeScaleAspectFill;
            [imageView setFrame:CGRectMake(0, 0, 210, 160)];
            [cell.mediaView addSubview:imageView];
//        }
    }
    
    return cell;
}




#pragma mark - Custom menu items

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
//    if (action == @selector(customAction:)) {
//        return YES;
//    }
//    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
//    if (action == @selector(customAction:)) {
//        [self customAction:sender];
//        return;
//    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    if (indexPath.item % 3 == 0) {
        return kJSQMessagesCollectionViewCellLabelHeightDefault;
    }
    
    return 0.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [self.demoData.messages objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.demoData.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    Offset +=Limit;
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"token": [NSUserDefaults userToken], @"user_id": [NSUserDefaults userID], @"room_id": _roomID, @"offset": @(Offset), @"limit": @(Limit)};
    [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIgetmessages] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
//        int resultSize = messages.count;
        if ([[responseObject valueForKey:@"status"] isEqualToString:@"failed"] && ![[responseObject valueForKey:@"response"] isEqualToString:@"No messages"]) {
//            if (![[responseObject valueForKey:@"response"] isEqualToString:@"No messages"]){
                self.inputToolbar.hidden = YES;
                self.showLoadEarlierMessagesHeader = NO;
                [HUD hide:YES];
                [self showMessage:NSLocalizedString(@"Sorry, you are banned in this chat.", nil) title:@"Error" buttonCount:1];
                [rusSocket close];
//            }
        }
        else {
            self.inputToolbar.hidden = NO;
            self.showLoadEarlierMessagesHeader = YES;
        int newSize = 0;
        NSArray *messagesData = [[[responseObject valueForKey:@"messages"] reverseObjectEnumerator] allObjects];
        if (messagesData.count > 0) {
            
            for (int i = 0; i < messagesData.count; i++) {
                [avatarsIMGs addObject:[messagesData[i] valueForKey:@"avatar"]];
                NSTimeInterval interval = [[messagesData[i] valueForKey:@"unix_time"] doubleValue];
                JSQMessage*message;
                if ([[messagesData[i] valueForKey:@"type"] isEqualToString:@"Text"]){
                    message = [[JSQMessage alloc] initWithSenderId:[messagesData[i] valueForKey:@"user_id"]
                                                 senderDisplayName:[messagesData[i] valueForKey:@"login"]
                                                              date:[NSDate dateWithTimeIntervalSince1970:interval]
                                                              text:[messagesData[i] valueForKey:@"data"]];
                }
                else if ([[messagesData[i] valueForKey:@"type"] isEqualToString:@"Photo"]){
                    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@""]];
                    
                    message = [[JSQMessage alloc] initWithSenderId:[messagesData[i] valueForKey:@"user_id"] senderDisplayName:[messagesData[i] valueForKey:@"login"] date:[NSDate dateWithTimeIntervalSince1970:interval] media:photoItem];
                    
                }
                else if ([[messagesData[i] valueForKey:@"type"] isEqualToString:@"Location"]){
                    NSString *aString = [messagesData[i] valueForKey:@"data"];
                    NSArray *arrayLOC = [aString componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                    arrayLOC = [arrayLOC filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF != ''"]];
                    CLLocation *ferryBuildingInSF = [[CLLocation alloc] initWithLatitude:[arrayLOC[0] doubleValue] longitude:[arrayLOC[1] doubleValue]];
                    
                    JSQLocationMediaItem *locationItem = [[JSQLocationMediaItem alloc] init];
                    [locationItem setLocation:ferryBuildingInSF withCompletionHandler:^{ [self.collectionView reloadData]; } ];
                    message = [[JSQMessage alloc] initWithSenderId:[messagesData[i] valueForKey:@"user_id"] senderDisplayName:[messagesData[i] valueForKey:@"login"] date:[NSDate dateWithTimeIntervalSince1970:interval] media:locationItem];
                    
                }
                [messagesID insertObject:[messagesData[i] valueForKey:@"message_id"] atIndex:0];
                [types insertObject:[messagesData[i] valueForKey:@"type"] atIndex:0];
                [messages insertObject:[messagesData[i] valueForKey:@"data"] atIndex:0];
                [usersID insertObject:[messagesData[i] valueForKey:@"user_id"] atIndex:0];

                [self.demoData.messages insertObject:message atIndex:0];
                newSize ++;
            }
        }
        else {
            Offset -=Limit;
        }
//        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
//        for (int i = resultSize; i < resultSize + newSize; i++) {
//            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i
//                                                              inSection:0]];
//        }
//        [self.collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
        [self.collectionView reloadData];
//        [self scrollToBottomAnimated:YES];
        [HUD hide:YES];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [HUD hide:YES];
        Offset -= Limit;
        [self showMessage:NSLocalizedString(@"Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
        NSLog(@"Error: %@", error);
    }];
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    UIActionSheet *actSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Actions with user", nil)
                                                              delegate:self
                                                     cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                destructiveButtonTitle:NSLocalizedString(@"Block user in chat", nil)
                                                     otherButtonTitles:NSLocalizedString(@"Report user", nil),nil];
    actSheet.tag = [usersID[indexPath.row] integerValue];
    [actSheet showInView:self.view];
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *msg = [self.demoData.messages objectAtIndex:indexPath.item];
    if (msg.isMediaMessage) {
        MediaVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MediaVC"];
        vc.roomID = _roomID;
        vc.messageID = messagesID[indexPath.row];
        vc.type = types[indexPath.row];
        vc.data = messages[indexPath.row];
//        vc.roomName = [roomData valueForKey:@"title"];
//        vc.inFavs = [roomData valueForKey:@"inFavs"];
//        vc.isAdmin = [roomData valueForKey:@"isAdmin"];
//        vc.Radius = [roomData valueForKey:@"radius"];
//        vc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    NSLog(@"Tapped message bubble!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods


- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].image) {
        // If there's an image in the pasteboard, construct a media item with that image and `send` it.
        JSQPhotoMediaItem *item = [[JSQPhotoMediaItem alloc] initWithImage:[UIPasteboard generalPasteboard].image];
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:self.senderId
                                                 senderDisplayName:self.senderDisplayName
                                                              date:[NSDate date]
                                                             media:item];
        [self.demoData.messages addObject:message];
        [self finishSendingMessage];
        return NO;
    }
    return YES;
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

@end
