//
//  AddRoomVC.h
//  
//
//  Created by Михаил Луцкий on 23.08.15.
//
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface AddRoomVC : UIViewController <MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *placeName;
@property (weak, nonatomic) IBOutlet UIImageView *placeIMG;
@property (weak, nonatomic) IBOutlet UIButton *imageButton;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)completeAction:(id)sender;
- (IBAction)closeAction:(id)sender;

@end
