//
//  UsersVC.m
//  Around me
//
//  Created by Михаил Луцкий on 02.10.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "UsersVC.h"

@interface UsersVC () {
    NSArray *users;
    int limit, offset;
}

@end

@implementation UsersVC

static NSString * const reuseIdentifier = @"user";

- (void)viewDidLoad {
    [super viewDidLoad];
    offset = 0;
    limit = 50;
    [self loadData];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[UIImage imageNamed:@"Circled Chevron Left -50"]
                                             style:UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(onBack:)];
    self.navigationController.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
}

- (void)onBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void) loadData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@?token=%@&user_id=%@&room_id=%@&limit=%d&offset=%d", kBaseAPIurl, kAPIuserslist, [NSUserDefaults userToken], [NSUserDefaults userID], _roomID, limit, offset] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        users = [responseObject valueForKey:@"response"];
        self.title = [NSString stringWithFormat:@"%@: %lu", NSLocalizedString(@"USERS", nil), (unsigned long)users.count];
        [self.collectionView reloadData];
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return users.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UsersViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    NSArray *userData = users[indexPath.row];
    cell.userLogin.text = [userData valueForKey:@"login"];
    [cell.userPicture setImageWithURL:[NSURL URLWithString:[userData valueForKey:@"avatar_url"]] key:nil placeholder:nil completionBlock:nil failureBlock:nil];
    // Configure the cell
    
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
