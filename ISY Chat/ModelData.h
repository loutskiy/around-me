#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <JSQMessagesViewController/JSQMessages.h>

@interface ModelData : NSObject

@property (strong, nonatomic) NSMutableArray *messages;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSDictionary *users;

//- (void)addPhotoMediaMessage;
//
//- (void)addLocationMediaMessageCompletion:(JSQLocationMediaItemCompletionBlock)completion;
//
//- (void)addVideoMediaMessage;

@end
