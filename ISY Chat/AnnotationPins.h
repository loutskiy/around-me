//
//  AnnotationPins.h
//  Around me
//
//  Created by Михаил Луцкий on 08/10/15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyAnnotation : NSObject <MKAnnotation>
@property (nonatomic) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title, *subtitle, *roomID;
- (id)initWithLocation:(CLLocationCoordinate2D)coord;
@end
