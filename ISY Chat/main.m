//
//  main.m
//  ISY Chat
//
//  Created by Михаил Луцкий on 17.08.15.
//  Copyright (c) 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
