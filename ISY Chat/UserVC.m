//
//  UserVC.m
//  
//
//  Created by Михаил Луцкий on 24.08.15.
//
//

#import "UserVC.h"
#import <AFNetworking/AFNetworking.h>
#import "UserCache.h"
#import "UIAlertMessage.h"
#import "UIColor+ISYColors.h"
#import "Constants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "PKImagePickerViewController.h"
#import <JMImageCache/JMImageCache.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface UserVC () <MBProgressHUDDelegate, MFMailComposeViewControllerDelegate> {
    MBProgressHUD *HUD;
}

@end

@implementation UserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    _userPicture.layer.cornerRadius = _userPicture.frame.size.width / 2;
    _userPicture.clipsToBounds = YES;
    _userPicture.layer.borderWidth = 3;
    _userPicture.layer.borderColor =[[UIColor ArounMeColor] CGColor];
    _userLogin.text = [NSUserDefaults userLogin];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    NSLog(@"User Picture %@", [NSUserDefaults userPicture]);
    [_showNews setOn:[NSUserDefaults showNewsBOOL]];
    [_userPicture setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[NSUserDefaults userPicture]]] key:nil placeholder:[UIImage imageNamed:@"profile"] completionBlock:nil failureBlock:nil];

}

- (IBAction)cancelButton:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)confirmPassword:(id)sender {
    if (![_passwordField.text isEqualToString:@""]) {
        if ([_passwordField.text isEqualToString:_rePasswordField.text]) {
            HUD = [[MBProgressHUD alloc] initWithView:self.view];
            [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
            HUD.dimBackground = NO;
            HUD.color = [UIColor ArounMeColor];
            HUD.delegate = self;
            [HUD show:YES];
            AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
            NSDictionary *parameters = @{@"new_password": _passwordField.text, @"token": [NSUserDefaults userToken], @"user_id": [NSUserDefaults userID]};
            [manager POST:[NSString stringWithFormat:@"%@%@", kBaseAPIurl, kAPIchangepassword] parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
                [HUD hide:YES];
                if ([[responseObject valueForKey:@"status"] isEqualToString:@"success"]){
                    [self showMessage:NSLocalizedString(@"Password is being changed.", nil) title:NSLocalizedString(@"Success", nil) buttonCount:1];
                }
                else {
                    [self showMessage:NSLocalizedString(@"It seems the old password is incorrect.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
                }
                NSLog(@"JSON: %@", responseObject);
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [HUD hide:YES];
                [self showMessage:NSLocalizedString(@"Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
                NSLog(@"Error: %@", error);
            }];
        }
        else {
            [self showMessage:NSLocalizedString(@"It seems the password is not equal with the re password field.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
        }
    }
    else {
        [self showMessage:NSLocalizedString(@"Fill in all the fields.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0 && indexPath.row == 0){
        UIAlertView *alertView = [UIAlertView alloc];
        alertView = [alertView initWithTitle:NSLocalizedString(@"Change user picture", nil) message:NSLocalizedString(@"Do you want to change user picture?", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel",nil) otherButtonTitles:NSLocalizedString(@"Change", nil), nil];
        alertView.tag = 1;
        [alertView show];
    }
    if (indexPath.section == 2){
        switch (indexPath.row) {
            case 0:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://bigbadbird.ru/around-me/privacy_policy.php?lang=%@", NSLocalizedString(@"en", nil)]]];
                break;
            case 1:{
                NSString *emailTitle = NSLocalizedString(@"Report message from Around me", nil);
                NSString *messageBody = NSLocalizedString(@"\n\nSend from Around me app for iOS", nil);
                NSArray *toRecipents = [NSArray arrayWithObject:@"support@lwts.ru"];
                MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                mc.mailComposeDelegate = self;
                [mc setSubject:emailTitle];
                [mc setMessageBody:messageBody isHTML:NO];
                [mc setToRecipients:toRecipents];
                
                [self presentViewController:mc animated:YES completion:NULL];
            }
                break;
            case 2:
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString: kURLwebsite]];
                break;
        }
    }
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
//    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
//    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
//                                                           [UIColor whiteColor], NSForegroundColorAttributeName,
//                                                           [UIFont fontWithName:@"HelveticaNeue" size:17], NSFontAttributeName, nil]];
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 1) {
//            PKImagePickerViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"SomeIdentifier"];
//            [self.navigationController presentModalViewController:controller animated:YES];

            PKImagePickerViewController *vc=[[PKImagePickerViewController alloc]init];
            vc.typePhoto = @"avatar";
            [self.navigationController presentViewController:vc animated:YES completion:nil];
//            [self.navigationController presentModalViewController:mainViewController animated:YES];
NSLog(@"1");
        }
    }
}
- (IBAction)logout:(id)sender {
    [[JMImageCache sharedCache] removeImageForURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[NSUserDefaults userPicture]]]];
    [NSUserDefaults changeAuth:false];
    [self dismissViewControllerAnimated:YES completion:nil];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)showNewsAction:(id)sender {
    [NSUserDefaults changeShowNews:_showNews.on];
}
@end
