//
//  MapVC.h
//  ISY Chat
//
//  Created by Михаил Луцкий on 20.09.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import "SMCalloutView.h"

@interface MapVC : UIViewController <MKMapViewDelegate, SMCalloutViewDelegate>
- (IBAction)getUserLocation:(id)sender;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) SMCalloutView *calloutView;
@property (nonatomic, strong) MKPointAnnotation *annotationForSMCalloutView, *annotationForUICalloutView;
- (IBAction)refreshMap:(id)sender;

@end
