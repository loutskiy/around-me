//
//  AnnotationPins.m
//  Around me
//
//  Created by Михаил Луцкий on 08/10/15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "AnnotationPins.h"

@implementation MyAnnotation
- (id)initWithLocation: (CLLocationCoordinate2D) coord {
    self = [super init];
    if (self) {
        self->_coordinate = coord;
    }
    return self;
}
@end