//
//  UsersViewCell.m
//  Around me
//
//  Created by Михаил Луцкий on 03.10.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "UsersViewCell.h"
#import "UIColor+ISYColors.h"

@implementation UsersViewCell

- (void)awakeFromNib {
    _userPicture.layer.cornerRadius = _userPicture.frame.size.width / 2;
    _userPicture.clipsToBounds = YES;
    _userPicture.layer.borderWidth = 3;
    _userPicture.layer.borderColor =[[UIColor ArounMeColor] CGColor];
    // Initialization code
}

@end
