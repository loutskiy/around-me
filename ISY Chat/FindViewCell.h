//
//  FindViewCell.h
//  
//
//  Created by Михаил Луцкий on 13.09.15.
//
//

#import <UIKit/UIKit.h>

@interface FindViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameConversation;
@property (weak, nonatomic) IBOutlet UILabel *countPeople;
@property (weak, nonatomic) IBOutlet UILabel *distanceToRoom;

@end
