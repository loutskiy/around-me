//
//  EditRoomVC.h
//  Around me
//
//  Created by Михаил Луцкий on 07/10/15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface EditRoomVC : UIViewController
<MKMapViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *placeName;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
- (IBAction)completeAction:(id)sender;
@property (strong, nonatomic) NSString *roomName;
@property (strong, nonatomic) NSString *Latitude;
@property (strong, nonatomic) NSString *Longitude;
@property (strong, nonatomic) NSString *radius;
@property (strong, nonatomic) NSString *roomID;
@property (nonatomic,retain) CLLocationManager *locationManager;

@end
