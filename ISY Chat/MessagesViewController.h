#import <JSQMessagesViewController/JSQMessages.h>
#import "ModelData.h"
#import <SocketRocket/SRWebSocket.h>
#import "UserCache.h"
#import "Constants.h"
#import "ContextMenuCell.h"
#import "YALContextMenuTableView.h"
#import "YALNavigationBar.h"
#import <STPopup/STPopup.h>
#import <AFNetworking/AFNetworking.h>
#import "UIAlertMessage.h"
#import <JMImageCache/JMImageCache.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "UsersVC.h"
#import "PKImagePickerViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <Parse/Parse.h>
#import "EditRoomVC.h"
#import "MediaVC.h"

static NSString *const menuCellIdentifier = @"rotationCell";

@interface MessagesViewController : JSQMessagesViewController <
        UIActionSheetDelegate,
        JSQMessagesComposerTextViewPasteDelegate,
        SRWebSocketDelegate,
        UITableViewDataSource,
        UITableViewDelegate,
        YALContextMenuTableViewDelegate,
        MBProgressHUDDelegate,
        MFMailComposeViewControllerDelegate,
        CLLocationManagerDelegate,
        UITextFieldDelegate,
        UIActionSheetDelegate
    >
@property (strong, nonatomic) NSString *roomName;
@property (strong, nonatomic) NSString *Latitude;
@property (strong, nonatomic) NSString *Radius;
@property (strong, nonatomic) NSString *Longitude;
@property (strong, nonatomic) NSString *roomID;
@property (strong, nonatomic) NSString *inFavs;
@property (strong, nonatomic) NSString *isAdmin;
@property (nonatomic, strong) YALContextMenuTableView* contextMenuTableView;
@property (nonatomic,retain) CLLocationManager *locationManager;
@property (nonatomic, strong) NSArray *menuTitles;
@property (nonatomic, strong) NSArray *menuIcons;
@property (strong, nonatomic) ModelData *demoData;
@property (assign) UITapGestureRecognizer *tapRecognizer;
@property (strong, nonatomic) IBOutlet UIView *viewMain;

@end
