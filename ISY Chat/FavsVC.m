//
//  FavsVC.m
//  Around me
//
//  Created by Михаил Луцкий on 28.09.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "FavsVC.h"

@interface FavsVC () <CLLocationManagerDelegate, MBProgressHUDDelegate> {
    float latUser, lonUser;
    int offset, limit;
    NSArray *rooms;
    BOOL internetConnection;
    MBProgressHUD *HUD;
    UIRefreshControl *refreshControl;
//    NSMutableArray *roomsNew;
}
@property (nonatomic,retain) CLLocationManager *locationManager;

@end

@implementation FavsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl setBackgroundColor:[UIColor groupTableViewBackgroundColor]];
    [refreshControl addTarget:self action:@selector(loadData) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

- (void)viewWillAppear:(BOOL)animated {
    if (![NSUserDefaults isAuth]) {
        [self dismissViewControllerAnimated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:YES];
    }
//    roomsNew = [[NSMutableArray alloc] init];
    offset = 0;
    limit = 25;
    internetConnection = true;
    HUD = [[MBProgressHUD alloc] initWithView:self.view];
    [[[UIApplication sharedApplication] keyWindow] addSubview:HUD];
    HUD.dimBackground = NO;
    HUD.color = [UIColor ArounMeColor];
    HUD.delegate = self;
    [HUD show:YES];
    __weak FavsVC *weakSelf = self;
    [self.tableView addInfiniteScrollingWithActionHandler:^{
        [weakSelf insertRowAtBottom];
        
    }];
    [self startLocationReporting];
}

- (void)insertRowAtBottom {
    __weak FavsVC *weakSelf = self;
    limit = limit+25;
    [weakSelf.tableView beginUpdates];
    [self loadData];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    FindViewCell *cell = (FindViewCell *)[tableView dequeueReusableCellWithIdentifier:@"conversation"];
    NSArray *roomData = rooms[indexPath.row];
    // Configure Cell
    [cell.distanceToRoom setText:[self metersConverter:[[roomData valueForKey:@"meters"] floatValue]]];
    [cell.nameConversation setText:[roomData valueForKey:@"title"]];
    [cell.countPeople setText:[roomData valueForKey:@"usersCount"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    MessagesViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatVC"];
    NSArray *roomData = rooms[indexPath.row];
    vc.roomID = [roomData valueForKey:@"room_id"];
    vc.Latitude = [roomData valueForKey:@"latitude"];
    vc.Longitude = [roomData valueForKey:@"longitude"];
    vc.roomName = [roomData valueForKey:@"title"];
    vc.isAdmin = [roomData valueForKey:@"isAdmin"];
    vc.Radius = [roomData valueForKey:@"radius"];
    vc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)viewWillDisappear:(BOOL)animated {
    [_locationManager stopUpdatingLocation];
}

- (void) loadData {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *request = [NSString stringWithFormat:@"%@%@?token=%@&user_id=%@&latitude=%f&longitude=%f&offset=%i&limit=%i", kBaseAPIurl,kAPIgetfavs, [NSUserDefaults userToken],[NSUserDefaults userID], latUser, lonUser, offset, limit];
    NSLog(@"request %@",request);
    [manager GET:request parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        internetConnection = true;
        if ([[responseObject valueForKey:@"status"] isEqualToString:@"success"]){
            rooms = [responseObject valueForKey:@"response"];
//            for (int i = 0; i < rooms.count; i++) {
//                [roomsNew addObject:rooms[i]];
//            }
            [self.tableView reloadData];
        }
        else {
            rooms = nil;
            [self.tableView reloadData];
            [self showMessage:NSLocalizedString(@"You haven't any favourites chats.", nil) title:NSLocalizedString(@"Error", nil) buttonCount:1];
        }
        [self stopUpdate];
        NSLog(@"JSON: %@", responseObject);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (internetConnection){
            internetConnection = false;
            [self showMessage:NSLocalizedString(@"Cannot update rooms. Check your internet connection.", nil) title:NSLocalizedString(@"Connection refused", nil) buttonCount:1];
        }
        [self stopUpdate];
        NSLog(@"Error: %@", error);
    }];
}

- (void) stopUpdate {
    __weak FavsVC *weakSelf = self;
    [refreshControl endRefreshing];
    [HUD hide:YES];
    [weakSelf.tableView.infiniteScrollingView stopAnimating];
    [weakSelf.tableView endUpdates];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return rooms.count;
}
- (void)startLocationReporting {
    NSLog(@"startLocation");
    _locationManager = [[CLLocationManager alloc] init];
    _locationManager.delegate = self;//or whatever class you have for managing location
    [self.locationManager setDesiredAccuracy:kCLLocationAccuracyBestForNavigation];
    _locationManager.distanceFilter =  kCLDistanceFilterNone;
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
    }else{
        [self.locationManager startUpdatingLocation];
    }//    [_locationManager startMonitoringSignificantLocationChanges];
}
-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch (status) {
        case kCLAuthorizationStatusNotDetermined:
        case kCLAuthorizationStatusRestricted:
        case kCLAuthorizationStatusDenied:
        {
            // do some error handling
        }
            break;
        default:{
            [self.locationManager startUpdatingLocation];
        }
            break;
    }
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    //    NSLog(@"locationManager");
    if (!(oldLocation.coordinate.latitude == newLocation.coordinate.latitude && oldLocation.coordinate.longitude == newLocation.coordinate.longitude)) {
        latUser = newLocation.coordinate.latitude;
        lonUser= newLocation.coordinate.longitude;
        NSLog(@"coord %f %f", latUser, lonUser);
        [self loadData];
    }
    
    //    [_locationManager stopUpdatingLocation];
}

- (NSString*) metersConverter: (float) metersValue {
    NSString *response;
    if (metersValue >= 1000) {
        metersValue /= 1000;
        response = [NSString stringWithFormat:@"%.f%@",metersValue, NSLocalizedString(@"km", nil)];
    }
    else {
        response = [NSString stringWithFormat:@"%.f%@",metersValue, NSLocalizedString(@"m", nil)];
    }
    return response;
}

@end
