//
//  AuthVC.h
//  ISY Chat
//
//  Created by Михаил Луцкий on 19.08.15.
//  Copyright (c) 2015 LWTS Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthVC : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *signupButton;
- (IBAction)loginAction:(id)sender;
- (IBAction)signupAction:(id)sender;
- (IBAction)privacyPolitic:(id)sender;

@end
