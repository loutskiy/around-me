//
//  UserVC.h
//  
//
//  Created by Михаил Луцкий on 24.08.15.
//
//

#import <UIKit/UIKit.h>

@interface UserVC : UITableViewController
@property (weak, nonatomic) IBOutlet UIImageView *userPicture;
- (IBAction)cancelButton:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;
@property (weak, nonatomic) IBOutlet UITextField *rePasswordField;
- (IBAction)confirmPassword:(id)sender;
- (IBAction)logout:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *userLogin;
@property (weak, nonatomic) IBOutlet UISwitch *showNews;
- (IBAction)showNewsAction:(id)sender;

@end
