//
//  UserCache.h
//  ISY Chat
//
//  Created by Михаил Луцкий on 25.07.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserDefaults (UserCache)

+(NSString *) userToken;
+(NSString *) userID;
+(NSString *) userLogin;
+(NSString *) userPicture;
+(NSString *) userType;
+(BOOL) isAuth;
+(NSString *) showNews;
+(BOOL) isDemo;
+(BOOL) showNewsBOOL;
+(void) changeAuth:(BOOL)auth;
+(void) changeShowNews:(BOOL)auth;
+(void) setValue:(NSString*)value forKey:(NSString*)key;
@end
