//
//  UIColor+ButerColors.h
//  Buter
//
//  Created by Михаил Луцкий on 25.07.15.
//  Copyright © 2015 AppCoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#define UIColorFromRGBA(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:a]
#define UIColorFromRGB(rgbValue) \
[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 \
alpha:1.0]
@interface UIColor (ISYColors)

+ (instancetype)sensationsPurpureColor;
+ (instancetype)buterBrownColor;
+ (instancetype)sensationsLightGrayColor;
+ (instancetype)sensationsDarkGrayColor;
+ (instancetype)ISYYellowColor;
+ (instancetype)ArounMeColor;
@end
