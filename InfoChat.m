//
//  PopupViewController2.m
//  STPopup
//
//  Created by Kevin Lin on 11/9/15.
//  Copyright (c) 2015 Sth4Me. All rights reserved.
//

#import "InfoChat.h"
#import "STPopup.h"
#import <GoogleMaps/GoogleMaps.h>
#import <JMImageCache/JMImageCache.h>
#import <MapKit/MapKit.h>
@implementation InfoChat {
    GMSGeocoder *geocoder_;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    geocoder_ = [[GMSGeocoder alloc] init];
    self.contentSizeInPopup = CGSizeMake(300, 200);
    self.landscapeContentSizeInPopup = CGSizeMake(400, 200);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didInfo:)
                                                 name:@"ChatInfo"
                                               object:nil];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"World Map-50"]
                                                                              style:UIBarButtonItemStyleDone
                                                                             target:self
                                                                             action:@selector(mapButtonOpen)];
}

- (void)viewWillAppear:(BOOL)animated {
    CLLocationCoordinate2D coord;
    coord.latitude =[Latitude doubleValue];
    coord.longitude = [Longitude doubleValue];
    _nameConversation.text = title;
    _mapPicture.contentMode = UIViewContentModeScaleAspectFill;
    [_mapPicture setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/staticmap?center=%@,%@&zoom=16&size=%0dx%0.f&scale=2&sensor=true&markers=%@,%@", Latitude, Longitude,300,self.mapPicture.frame.size.width, Latitude, Longitude]] key:nil placeholder:nil completionBlock:nil failureBlock:nil];
    GMSReverseGeocodeCallback handler = ^(GMSReverseGeocodeResponse *response, NSError *error) {
        NSLog(@"google %@", response.firstResult);
        GMSAddress *addressGMS = response.firstResult;
        if ( (addressGMS) || ([addressGMS valueForKey:@"thoroughfare"] != nil) ) {
            _adressPlace.text = [NSString stringWithFormat:@"%@",[addressGMS valueForKey:@"thoroughfare"]];
        } else {
            _adressPlace.text = NSLocalizedString(@"In the middle of nowhere", nil);
        }
        _adressPlace.hidden = false;
    };
    [geocoder_ reverseGeocodeCoordinate:coord completionHandler:handler];
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}
- (void)didInfo:(NSNotification*)notification {

    NSDictionary *chatInfo = [notification object];
    _nameConversation.text = [chatInfo objectForKey:@"Title"];
    self.title = @"";
    title = [chatInfo objectForKey:@"Title"];
    Latitude = [chatInfo objectForKey:@"Latitude"];
    Longitude = [chatInfo objectForKey:@"Longitude"];
    NSLog(@"%@", [chatInfo objectForKey:@"Title"]);
      
}
- (void)mapButtonOpen
{
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake([Latitude doubleValue], [Longitude doubleValue]);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:title];
        
        // Set the directions mode to "Walking"
        // Can use MKLaunchOptionsDirectionsModeDriving instead
        NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeWalking};
        // Get the "Current User Location" MKMapItem
        MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
        // Pass the current location and destination map items to the Maps app
        // Set the direction mode in the launchOptions dictionary
        [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem]
                       launchOptions:launchOptions];
    }
}

@end
