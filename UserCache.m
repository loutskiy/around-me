//
//  UserCache.m
//  ISY Chat
//
//  Created by Михаил Луцкий on 25.07.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import "UserCache.h"

@implementation NSUserDefaults (UserCache)
+(instancetype) NSUserDefaultsString:(NSString*) key {
    return [[NSUserDefaults standardUserDefaults] valueForKey:key];
}
+(BOOL) NSUserDefaultsBool:(NSString*) key {
    return [[NSUserDefaults standardUserDefaults] boolForKey:key];
}
+(NSString *)userToken {
    return [self NSUserDefaultsString:@"Token"];
}
+(NSString *)userID {
    return [self NSUserDefaultsString:@"User_ID"];
}
+(NSString *)userLogin {
    return [self NSUserDefaultsString:@"Login"];
}
+(NSString *)userPicture {
    return [self NSUserDefaultsString:@"Picture"];
}
+(NSString *)userType {
    return [self NSUserDefaultsString:@"Type"];
}
+(BOOL)isAuth {
    return [self NSUserDefaultsBool:@"auth"];
}
+(BOOL)isDemo {
    return [self NSUserDefaultsBool:@"demo"];
}
+(BOOL)showNewsBOOL {
    return [self NSUserDefaultsBool:@"news"];
}
+(NSString *)showNews {
    if ([self NSUserDefaultsBool:@"news"]) {
        return @"true";
    }
    else {
        return @"false";
    }
}
+(void)changeAuth:(BOOL)auth {
    NSUserDefaults *userCache = [NSUserDefaults standardUserDefaults];
    [userCache setBool:auth forKey:@"auth"];
    [userCache synchronize];
}
+(void)changeShowNews:(BOOL)auth {
    NSUserDefaults *userCache = [NSUserDefaults standardUserDefaults];
    [userCache setBool:auth forKey:@"news"];
    [userCache synchronize];
}
+(void)setValue:(NSString *)value forKey:(NSString *)key {
    NSUserDefaults *userCache = [NSUserDefaults standardUserDefaults];
    [userCache setValue:value forKey:key];
    [userCache synchronize];
}
@end
