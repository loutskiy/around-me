//
//  PopupViewController2.h
//  STPopup
//
//  Created by Kevin Lin on 11/9/15.
//  Copyright (c) 2015 Sth4Me. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoChat : UIViewController {
    NSString *title, *Latitude, *Longitude;
}

@property (weak, nonatomic) IBOutlet UIImageView *mapPicture;
@property (weak, nonatomic) IBOutlet UILabel *adressPlace;
@property (weak, nonatomic) IBOutlet UILabel *nameConversation;

@end
