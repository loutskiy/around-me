//
//  KeyboardBar.m
//  KeyboardInputView
//
//  Created by Brian Mancini on 10/4/14.
//  Copyright (c) 2014 iOSExamples. All rights reserved.
//

#import "KeyboardBar.h"
#import "UIColor+ISYColors.h"
@implementation KeyboardBar

- (id)initWithDelegate:(id<KeyboardBarDelegate>)delegate {
    self = [self init];
    self.delegate = delegate;
    return self;
}

- (id)init {
    CGRect screen = [[UIScreen mainScreen] bounds];
    CGRect frame = CGRectMake(0,0, CGRectGetWidth(screen), 40);
    self = [self initWithFrame:frame];
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if(self) {
        
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [[UIColor grayColor] CGColor];
        self.layer.borderWidth = 0.5;
        self.viewPlace = [[UIView alloc]initWithFrame:CGRectMake(5, 5, frame.size.width - 80, frame.size.height - 10)];
        self.viewPlace.layer.cornerRadius = 8;
        self.viewPlace.layer.borderColor = [UIColorFromRGB(0xcecece) CGColor];
        self.viewPlace.layer.borderWidth = 0.5;
        self.viewPlace.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:self.viewPlace];
 
        self.textField = [[UITextField alloc]initWithFrame:CGRectMake(10, 6, frame.size.width - 88, frame.size.height - 12)];
        self.textField.placeholder = @"Введите сообщение";
        self.textField.font = [UIFont systemFontOfSize:14];
        self.textField.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [self addSubview:self.textField];
        

//        self.textView = [[UITextView alloc]initWithFrame:CGRectMake(5, 5, frame.size.width - 80, frame.size.height - 10)];
//        self.textView.layer.cornerRadius = 5;
//        self.textView.backgroundColor = [UIColor colorWithRed:222/255.0 green:222/255.0 blue:222/255.0 alpha:1.0];
//        [self addSubview:self.textView];
        
        self.actionButton = [[UIButton alloc]initWithFrame:CGRectMake(frame.size.width - 70, 5, 65, frame.size.height - 10)];
//        self.actionButton.backgroundColor = [UIColor buterBrownColor];
        self.actionButton.layer.cornerRadius = 5.0;
//        self.actionButton.layer.borderWidth = 1.0;
//        self.actionButton.layer.borderColor = [[UIColor buterBrownColor] CGColor];
        [self.actionButton setTitleColor:[UIColor ArounMeColor] forState:UIControlStateNormal];
        [self.actionButton setTitle:@"Отпр." forState:UIControlStateNormal];
        [self.actionButton addTarget:self action:@selector(didTouchAction) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.actionButton];
        
    }
    return self;
}

- (void) didTouchAction
{
    [self.delegate keyboardBar:self sendText:self.textField.text];
}

@end
