//
//  UIColor+ButerColors.m
//  Buter
//
//  Created by Михаил Луцкий on 25.07.15.
//  Copyright © 2015 AppCoda. All rights reserved.
//

#import "UIColor+ISYColors.h"

@implementation UIColor (ISYColors)

+ (instancetype)colorWith255Red:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue {
    return [UIColor colorWithRed:red/255.0 green:green/255.0 blue:blue/255.0 alpha:1.0];
}

+ (instancetype)colorWith255Gray:(NSInteger)grayScale {
    return [self colorWith255Red:grayScale green:grayScale blue:grayScale];
}

+ (instancetype)sensationsPurpureColor {
    return UIColorFromRGB(0xF8585C);
}

+ (instancetype)buterBrownColor {
    return UIColorFromRGB(0x9C4D00);
}

+ (instancetype)sensationsLightGrayColor {
    return [self colorWith255Gray:246];
}

+ (instancetype)sensationsDarkGrayColor {
    return [self colorWith255Gray:210];
}

+ (instancetype)ISYYellowColor {
    return UIColorFromRGB(0xF3CF4F);
}

+ (instancetype) ArounMeColor {
    return UIColorFromRGB(0xA20022);
}

@end
