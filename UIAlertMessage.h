//
//  UIAlertMessage.h
//  ISY Chat
//
//  Created by Михаил Луцкий on 04.07.15.
//  Copyright © 2015 LWTS Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIViewController (UIAlertMessage)

- (void)showMessage:(NSString *)message title:(NSString*)title buttonCount:(NSInteger)actionCount;
- (void)showMessage:(NSString *)message title:(NSString*)title buttonCount:(NSInteger)actionCount action:(NSString*)action;
- (void)showMessage:(NSString *)message title:(NSString*)title buttonCount:(NSInteger)actionCount action2:(NSString*)action;

@end